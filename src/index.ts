import { LionButton } from '@lion/ui/button.js';
import { SlotMixin } from '@lion/ui/core.js';
import '@lion/ui/types/core.js'; // Uncomment for workaround
// import { LionButton } from '@lion/button';
// import { SlotMixin } from '@lion/core';

export class MyButton extends SlotMixin(LionButton) {
    get slots() {
		return {};
	}
};