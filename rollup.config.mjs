import { nodeResolve } from '@rollup/plugin-node-resolve';
import typescript from '@rollup/plugin-typescript';

const rootDir = './src';
const dir = './dist';
const declarationDir = 'dist/temp/src';

export default {
	input: {
		index: 'src/index.ts',
	},
	output: {
		entryFileNames: '[name].js',
		dir,
		format: 'es',
		preserveModules: true,
		sourcemap: true,
	},
	external: [/@lion\//],
	plugins: [
		nodeResolve(),
		typescript({
			rootDir,
			declaration: true,
			declarationDir,
		}),
	],
}
